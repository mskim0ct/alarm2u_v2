- 진행중인 프로젝트 입니다. 


# 적용 패턴 
- 안드로이드 권장 아키텍처
<img src="image/architecture.jpg"  width="800" height="400">

# 라이브러리 
- databinding(AAC) - 양방향 데이터 바인딩
- LiveData(AAC)
- room(AAC)
- RxJava, RxAndroid - Single
- retrofit2 - Single

# 앱 특징 설명
- YouTube 영상 알람 앱
- swipe를 통한 알람 삭제
- YouTube 영상 대신 음악파일로 알람 수행 가능
