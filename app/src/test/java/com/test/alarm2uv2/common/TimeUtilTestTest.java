package com.test.alarm2uv2.common;

import org.junit.Test;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtilTestTest{
    @Test
    public void main() {
        long time = 1612331400000L;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd, HH:mm:ss:SSS");
        Date date = new Date(time);
        System.out.println(sdf.format(date));
    }
}