package com.test.alarm2uv2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

//import com.test.alarm2uv2.CombineLiveData;
import com.test.alarm2uv2.common.SingleLiveEvent;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.model.ItemRepository;
import com.test.alarm2uv2.model.resource.db.Item;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class YouTubeItemViewModel extends AndroidViewModel {

    private final ItemRepository itemRepository;
    public MutableLiveData<Item> item;
    public SingleLiveEvent<Void> finish;
    public MutableLiveData<Boolean> showActivity ;
    public YouTubeItemViewModel(@NonNull Application application) {
        super(application);
        itemRepository = new ItemRepository(application);
        item = new MutableLiveData<>();
        finish = new SingleLiveEvent<>();
        showActivity = new MutableLiveData<>();
    }

    public List<Item> getItems(){
        return itemRepository.getItems();
    }
    public void setItem(Item item){
        this.item.setValue(item);
    }

    public void updateItem(Item item){
        itemRepository.updateItem(item);
    }
    public void onClickSnooze(){
        item.getValue().setAlarmTime(Util.getCurrentTime()+600000);
        item.getValue().setSnooze(true);
        item.getValue().setRunFlag(true);
        finish.call();
    }

    public void onClickDone(){
        if(Util.isOnceAlarm(item.getValue().getDays())){
            item.getValue().setAlarmTime(item.getValue().getInitAlarmTime());
            item.getValue().setSnooze(false);
            item.getValue().setRunFlag(false);
        }else{
            long alarmTime = Util.nextAlarmTimeRepeat(item.getValue().getInitAlarmTime(), item.getValue().getDays());
            item.getValue().setInitAlarmTime(alarmTime);
            item.getValue().setAlarmTime(alarmTime);
            item.getValue().setSnooze(false);
            item.getValue().setRunFlag(true);
        }

        finish.call();
    }
}
