package com.test.alarm2uv2.viewmodel;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.test.alarm2uv2.common.SingleLiveEvent;
import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.model.ItemRepository;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.model.resource.db.MusicItem;

import java.util.List;

import io.reactivex.Single;

public class DetailViewModel extends AndroidViewModel {

    private final ItemRepository itemRepository;
    private final MutableLiveData<Integer> mode;
    private final MutableLiveData<Item> item;
    private final MutableLiveData<Integer> waitState;
    private final MutableLiveData<Integer> maxVolume;
    private final MutableLiveData<Integer> hour ;
    private final MutableLiveData<Integer> minute;
    private final MutableLiveData<List<MusicItem>> musicItems;

    public SingleLiveEvent<Void> openYouTubeDialog;
    public SingleLiveEvent<Void> openMusicDialog;
    public SingleLiveEvent<Void> openYouTubeApp;
    public SingleLiveEvent<Void> saveYouTubeInfo;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        itemRepository = new ItemRepository(application);
        mode = new MutableLiveData<>();
        item = new MutableLiveData<>();
        waitState = new MutableLiveData<>();
        waitState.setValue(View.GONE);
        maxVolume = new MutableLiveData<>();
        hour = new MutableLiveData<>();
        minute = new MutableLiveData<>();
        musicItems = new MutableLiveData<>();

        openYouTubeDialog = new SingleLiveEvent<>();
        openMusicDialog = new SingleLiveEvent<>();
        openYouTubeApp = new SingleLiveEvent<>();
        saveYouTubeInfo = new SingleLiveEvent<>();
    }

    public void setMode(int mode){
        this.mode.setValue(mode);
    }
    public MutableLiveData<Integer> getMode(){
        return mode;
    }
    public void setWaitState(int state){
        waitState.setValue(state);
    }
    public MutableLiveData<Integer> getWaitState(){
        return waitState;
    }

    public void setItem(Item item){
        this.item.setValue(item);
    }
    public MutableLiveData<Item> getItem(){
        return item;
    }
    public void setHour(int hour){
        this.hour.setValue(hour);
    }
    public MutableLiveData<Integer> getHour(){
        return hour;
    }
    public void setMinute(int minute){
        this.minute.setValue(minute);
    }
    public MutableLiveData<Integer> getMinute(){
        return minute;
    }

    public Single<String> callYouTubeInfo(String url){
        String uri = Util.getYouTubeUri(url);
        return itemRepository.callYouTubeInfo(uri);
    }

    public List<MusicItem> callMusicList(){
        return itemRepository.getMusicItems();
    }
    public void setMusicItems(List<MusicItem> items){
        musicItems.setValue(items);
    }
    public MutableLiveData<List<MusicItem>> getMusicItems(){
        return musicItems;
    }
    public void setMusicItem(MusicItem musicItem){
         if(musicItem == null){
             item.getValue().getMusicItem().setContentUri("");
             item.getValue().getMusicItem().setMusicTitle("");
             item.getValue().getMusicItem().setArtist("");
         }
        item.getValue().getMusicItem().setContentUri(musicItem.getContentUri());
        item.getValue().getMusicItem().setMusicTitle(musicItem.getMusicTitle());
        item.getValue().getMusicItem().setArtist(musicItem.getArtist());
    }
    public void deleteMusicItem(){
        item.getValue().getMusicItem().setContentUri("");
        item.getValue().getMusicItem().setMusicTitle("");
        item.getValue().getMusicItem().setArtist("");
    }
    public void onClickOpenYouTubeDialog(){
        openYouTubeDialog.call();
    }
    public void onClickOpenMusicDialog(){
        openMusicDialog.call();
    }
    public void setMaxVolume(int volume){
        maxVolume.setValue(volume);
    }
    public MutableLiveData<Integer> getMaxVolume(){
        return maxVolume;
    }

    public void onClickSaveItem(){
        long alarmTime;
        if(Util.isOnceAlarm(item.getValue().getDays())){
            alarmTime = Util.nextAlarmTimeOnce(getHour().getValue(), getMinute().getValue());
        }else{
            alarmTime = Util.nextAlarmTimeRepeat(getHour().getValue(), getMinute().getValue(), item.getValue().getDays());
        }
        item.getValue().setInitAlarmTime(alarmTime);
        item.getValue().setAlarmTime(alarmTime);
        item.getValue().setRunFlag(true);
        item.getValue().setUpdateTime(Util.getCurrentTime());
        if(mode.getValue() == CommonCode.CREATE){
            item.getValue().setRegistrationTime(Util.getCurrentTime());
        }
    }

    //youtube dialog
    public void onClickOpenYouTubeApp(){
        openYouTubeApp.call();
    }

    public void onClickSaveYouTubeInfo(String url){
        item.getValue().getYouTubeItem().setYouTubeUri(url);
        saveYouTubeInfo.call();
    }

    //save description
    public void setDescription(String description){
        item.getValue().setDescription(description);
    }
}
