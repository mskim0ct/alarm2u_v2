package com.test.alarm2uv2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.test.alarm2uv2.common.SingleLiveEvent;
import com.test.alarm2uv2.model.ItemRepository;
import com.test.alarm2uv2.model.resource.db.Item;

import java.util.List;

public class ItemViewModel extends AndroidViewModel{
    public static final String TAG = "ItemViewModel.class";

    private final ItemRepository itemRepository;
    //item 관련
    private final LiveData<List<Item>> items;
    private final MutableLiveData<Item> item;
    //버튼 관련
    public SingleLiveEvent<Void> detailBtn = new SingleLiveEvent<>();
    public SingleLiveEvent<Void> createBtn = new SingleLiveEvent<>();
    public SingleLiveEvent<Void> callPermissionOverlay = new SingleLiveEvent<>();
    //생성,수정 확인
    private final MutableLiveData<Integer> mode = new MutableLiveData<>();
    //permission overlay 설정여부
    private final MutableLiveData<Integer> permissionOverlayBanner = new MutableLiveData<>();

    public ItemViewModel(@NonNull Application application) {
        super(application);
        itemRepository = new ItemRepository(application);
        items = itemRepository.getLiveDataAllItems();
        item = new MutableLiveData<>();
    }

    public void setItem(Item item){
        this.item.setValue(item);
    }
    public MutableLiveData<Item> getItem(){
        return item;
    }
    public void insertItem(Item item){
        itemRepository.insertItem(item);
    }
    public void updateItem(Item item){
        itemRepository.updateItem(item);
    }
    public void deleteItem(Item item){
        itemRepository.deleteItem(item);
    }
    public void deleteItems(){
        itemRepository.deleteItems();
    }
    public LiveData<List<Item>> getAllItems(){
        return items;
    }
    public void onClickCreateView(){
        createBtn.call();
    }
    public void onClickDetailView(){
        detailBtn.call();
    }

    public void setMode(int mode){
        this.mode.setValue(mode);
    }
    public MutableLiveData<Integer> getMode(){
        return mode;
    }

    public void onClickPermissionOverlay(){
        callPermissionOverlay.call();
    }
    public void setPermissionOverlayBanner(int value){
        permissionOverlayBanner.setValue(value);
    }
    public MutableLiveData<Integer> getPermissionOverlayBanner(){
        return permissionOverlayBanner;
    }

}
