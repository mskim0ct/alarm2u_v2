package com.test.alarm2uv2.common.alarm;

import android.content.Context;

import com.test.alarm2uv2.model.resource.db.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*****************************************************
 * 알람 등록을 위한 클래스
 * : RunFlag -> TRUE && 가장 빠른 SnoozeAlarmTime 등록
 * : DB 에서 데이터를 가져오기만...
 *****************************************************/
public class AlarmController {
    private static final String TAG = "AlarmController.class";
    private final AlarmServiceManager alarmServiceManager;

    public AlarmController(Context context) {
        alarmServiceManager = new AlarmServiceManager(context);
    }

    public void createAlarm(List<Item> items){
        alarmServiceManager.cancel();
        Item item = findFirstItem(items);
        if(item == null){
            return ;
        }
        alarmServiceManager.register(item, item.getAlarmTime());

    }
    private Item findFirstItem(List<Item> items){

        //알람 없음
        if(items == null || items.isEmpty()){
            return null;
        }
        //실행가능한 알람 없음
        List<Item> executableAlarmItemList = new ArrayList<Item>();
        for(Item item : items){
            if(item.isRunFlag()){
                executableAlarmItemList.add(item);
            }
        }
        if(executableAlarmItemList.isEmpty()){
            return null;
        }
        //빠른 Item 찾기 - snoozeAlarm 으로 찾기
        Collections.sort(executableAlarmItemList, new Comparator<Item>(){
            @Override
            public int compare(Item o1, Item o2) {
                if(o1.getAlarmTime() < o2.getAlarmTime()){
                    return -1;
                }else if(o1.getAlarmTime() > o2.getAlarmTime()){
                    return 1;
                }else{
                    if(o1.getUpdateTime() > o2.getUpdateTime()){
                        return -1;
                    }else if(o1.getUpdateTime() < o2.getUpdateTime()){
                        return 1;
                    }else{
                        return 0;
                    }
                }
            }
        });
        return executableAlarmItemList.get(0);
    }

}
