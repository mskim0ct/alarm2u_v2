package com.test.alarm2uv2.common.alarm;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.receiver.AlarmReceiver;

/********************************
 * 알람 설정 클래스
 * 등록/취소
 * ****************************/

public class AlarmServiceManager {
    private final int ALARM_REQUEST_CODE = 1000;
    private final String ALARM_ACTION = "com.test.alarm2uv2.ALARM_RECEIVER";
    private final Context context;
    public AlarmServiceManager(Context context){
        this.context = context;
    }

    public void register(Item item, long time){
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(ALARM_ACTION);
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonCode.ITEM, item);
        intent.putExtra(CommonCode.ITEM, bundle);
        PendingIntent pendingIntent= PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(time, pendingIntent);
        alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);
    }

    public void cancel(){
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(ALARM_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if(pendingIntent != null){
            alarmManager.cancel(pendingIntent);
        }
    }
}
