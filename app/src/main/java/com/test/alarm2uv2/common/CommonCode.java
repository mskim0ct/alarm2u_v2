package com.test.alarm2uv2.common;

public class CommonCode {
    //알람 조작 관련 코드
    public static final int CREATE = 1;
    public static final int UPDATE = 2;
    public static final int DEFAULT = -1;
    public static final int OK = 200;

    //recyclerview footer
    public static final int FOOTER = 100;
    //String 관련 코드
    public static final String FLAG = "FLAG";
    public static final String ITEM = "ITEM";
    public static final String SET_ALARM = "ALARM";
    public static final String INIT_VOLUME="VOLUME";
    public static final String ITEM_ID="ITEM_ID";

    //YouTube Package Name
    public static String YOUTUBE_PACKAGE_NAME = "com.google.android.youtube";

    //Notifcation 관련 코드
    public static final int NOTIFICATION_ALARM_ID=10000;
    public static final int NOTIFICATION_ALARM_ERROR_ID=10001;
}
