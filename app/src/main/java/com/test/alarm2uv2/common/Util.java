package com.test.alarm2uv2.common;

import android.util.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.security.auth.callback.Callback;

public class Util {
/*    public static long convertTime(int hour, int minute){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }*/

    public static long getCurrentTime(){
        return System.currentTimeMillis();
    }

    public static int getCurrentMinute(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getCurrentTime());
        return calendar.get(Calendar.MINUTE);
    }

    public static int getCurrentHour(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getCurrentTime());
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

//    public static long getAlarmTime(long time, int[] days){
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(time);
//        return getAlarmTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), days);
//    }
//    public static long getAlarmTime(int hour, int minute, int[] days){
//        long curTime = System.currentTimeMillis();
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(curTime);
//        calendar.set(Calendar.HOUR_OF_DAY, hour);
//        calendar.set(Calendar.MINUTE, minute);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//
//        boolean repeat = false;
//        for(int i =  0; i < 7 ; i++){
//            if( days[i] == 1 ){
//                repeat = true;
//                break;
//            }
//        }
//        if(curTime >= calendar.getTimeInMillis()){
//            calendar.add(Calendar.DAY_OF_MONTH, 1);
//        }
//        //days X
//        if(!repeat){
//            return calendar.getTimeInMillis();
//        }
//        //days 0
//        int curDay = calendar.get(Calendar.DAY_OF_WEEK)-1;
//        for(int i = 0 ; i < 7 ; i++){
//            if(days[(curDay+i)%7] == 1){
//                calendar.add(Calendar.DATE, i);
//                break;
//            }
//        }
//        return calendar.getTimeInMillis();
//    }

    public static int getHour(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }
    public static int getMinute(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.get(Calendar.MINUTE);
    }
  /*  public static String textTime(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return String.format(Locale.getDefault(), "%02d : %02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }
    public static String textDays(List<Integer> days, long time){
        Calendar calendar = Calendar.getInstance();
        if(days == null || days.isEmpty()){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
            return sdf.format(new Date(time));
        }
        //TODO 일월화.... 추후 String.xml 에서 가져오도록 해
        String[] str_day = new String[]{"일", "월", "화", "수", "목", "금", "토" };
        StringBuilder sb  = new StringBuilder();
        for(Integer day : days){
            sb.append(str_day[day]+" ");
        }
        return sb.toString().trim();
    }*/
    // calculate next alarmTime when repeatTime set
    public static long nextSnoozeAlarmTime(long currentAlarmTime, int minute){
        return currentAlarmTime+TimeUnit.MINUTES.toMillis(minute);
    }
    // 알람 지났는지 확인
    public static boolean isPastAlarmTime(long time){
        long curTime = System.currentTimeMillis();
        return curTime >= time;
    }
    //반복 알람일 경우, 알람 시간 계산
    public static long nextAlarmTimeRepeat(int hour, int minute, int[] days){
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        if(System.currentTimeMillis() >= calendar.getTimeInMillis()){
            calendar.add(Calendar.DATE, 1);
        }
        int curDay = calendar.get(Calendar.DAY_OF_WEEK)-1;
        for(int i = 0 ; i < 7 ; i++){
            if( days[(curDay+i)%7] == 1){
                calendar.add(Calendar.DATE, i);
            }
        }
        return calendar.getTimeInMillis();
    }

    public static long nextAlarmTimeRepeat(long time, int[] days){
        return nextAlarmTimeRepeat(Util.getHour(time), Util.getMinute(time), days);
    }

    public static long nextAlarmTimeOnce(int hour, int minute){
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        if(calendar.getTimeInMillis() <= System.currentTimeMillis()){
            calendar.add(Calendar.DATE, 1);
        }
        return calendar.getTimeInMillis();
    }
    public static long nextAlarmTimeOnce(long time){
        int hour = Util.getHour(time);
        int minute = Util.getMinute(time);
        return nextAlarmTimeOnce(hour, minute);

    }

    public static boolean isOnceAlarm(int[] days){
        if(days == null){
            return true;
        }
        for(int i = 0 ; i < 7 ; i++){
            if(days[i] == 1){
                return false;
            }
        }
        return true;
    }

    public static String getYouTubeUri(String value){
        if(value == null || value.equals("")){
            return "";
        }
        String[] arr = value.split("/");
        return arr[arr.length-1];
    }
}
