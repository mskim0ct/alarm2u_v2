package com.test.alarm2uv2.view.youtube;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.test.alarm2uv2.common.Util;
import io.reactivex.Observable;

import io.reactivex.subjects.PublishSubject;

public class YouTubePlayerController{

    private final String TAG = "YouTubePlayerController.class";
    private YouTubePlayer youTubePlayer;

    private final AudioManager audioManager;
    private final int initVolumeSize;
    private String youtubeUri;
    private boolean error;
    private final PublishSubject<Boolean> publishSubject;

    public YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {
            Log.d(TAG, "youtube play loading.");
        }

        @Override
        public void onLoaded(String s) {
            if(!youTubePlayer.isPlaying()){
                youTubePlayer.play();
            }
        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {
            youTubePlayer.loadVideo(youtubeUri);
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            setError(true);
            Log.d(TAG, "play error : "+errorReason.name());
        }
    };

    public YouTubePlayerController(Context context){
        audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        initVolumeSize = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        error = false;
        publishSubject = PublishSubject.create();
    }

    public void setError(boolean error){
        this.error = error;
        publishSubject.onNext(error);
    }
    public boolean isError(){
        return error;
    }

    public Observable<Boolean> getObservable(){
        return publishSubject.hide();
    }

    public void play(String uri, int volumeSize){
        youtubeUri = Util.getYouTubeUri(uri);
        if(youtubeUri.equals("")){
            setError(true);
            return ;
        }
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeSize, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        youTubePlayer.loadVideo(youtubeUri);
    }

    public void release(){
        if(audioManager != null){
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, initVolumeSize, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        }
        if(youTubePlayer != null){
            youTubePlayer.release();
        }
        youTubePlayer = null;
    }

    public void setYouTubePlayer(YouTubePlayer youTubePlayer){
        this.youTubePlayer = youTubePlayer;
        this.youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        this.youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
    }
    public YouTubePlayer getYouTubePlayer(){
        return youTubePlayer;
    }



//    private YouTubePlayer youTubePlayer;
//    private YouTubeItemViewModel youTubeItemViewModel;
//    private String id;
//    private int initVolume;
//    private AudioManager audioManager;
//    public YouTubePlayerController(Context context, YouTubeItemViewModel youTubeItemViewModel){
//        this.youTubeItemViewModel = youTubeItemViewModel;
//        String uri = youTubeItemViewModel.item.getValue().getYouTubeItem().getYouTubeUri();
//        if(uri != null && uri.length() > 0){
//            String[] tmp = uri.split("/");
//            id = tmp[tmp.length-1];
//        }
//        audioManager = (AudioManager)context.getSystemService(context.AUDIO_SERVICE);
//        initVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//    }
//    public YouTubePlayer getYouTubePlayer(){
//        return youTubePlayer;
//    }
//    public void setYouTubePlayer(YouTubePlayer youTubePlayer){
//        this.youTubePlayer = youTubePlayer;
//    }
//    private YouTubePlayer.PlayerStateChangeListener stateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
//        @Override
//        public void onLoading() {
//            Log.d("TAG", "onLoading");
//        }
//
//        @Override
//        public void onLoaded(String s) {
//            Log.d(TAG, "onLoaded");
//            youTubePlayer.play();
//        }
//
//        @Override
//        public void onAdStarted() {
//
//        }
//
//        @Override
//        public void onVideoStarted() {
//
//        }
//
//        @Override
//        public void onVideoEnded() {
//            youTubePlayer.loadVideo(id);
//        }
//
//        @Override
//        public void onError(YouTubePlayer.ErrorReason errorReason) {
//            Log.d(TAG, "player error : "+errorReason.name());
//            youTubeItemViewModel.setYouTubePlayerError(true);
//        }
//    };
//    private YouTubePlayer.OnInitializedListener initializedListener = new YouTubePlayer.OnInitializedListener() {
//        @Override
//        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
//
//            Log.d(TAG, "onInitYoutubeSuccess");
//            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, youTubeItemViewModel.item.getValue().getVolumeSize(), AudioManager.FLAG_VIBRATE);
//            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
//            youTubePlayer.setPlayerStateChangeListener(stateChangeListener);
//            setYouTubePlayer(youTubePlayer);
//            if(id == null || id.length() <= 0){
//                youTubeItemViewModel.setYouTubePlayerError(true);
//                return ;
//            }
//            getYouTubePlayer().loadVideo(id);
//        }
//
//        @Override
//        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//            Log.d(TAG,"init error : "+youTubeInitializationResult.name() );
//            youTubeItemViewModel.setYouTubePlayerError(true);
//        }
//    };
//
//    public YouTubePlayer.OnInitializedListener getInitListener(){
//        return initializedListener;
//    }
//    public void release(){
//        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, initVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//        if(youTubePlayer != null){
//            youTubePlayer.release();
//            youTubePlayer = null;
//        }
//    }
}
