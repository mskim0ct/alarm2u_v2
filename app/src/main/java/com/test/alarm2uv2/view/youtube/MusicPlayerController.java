package com.test.alarm2uv2.view.youtube;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class MusicPlayerController {

    private final String TAG = "MusicPlayerController.class";

    private final Context context;
    private MediaPlayer mediaPlayer;
    private final AudioManager audioManager;

    private final int initVolume;

    private AudioFocusRequest audioFocusRequest;
    private AudioManager.OnAudioFocusChangeListener afChangeListener;

    private boolean error;
    private final PublishSubject<Boolean> errorSubject;
    public MusicPlayerController(Context context){
        this.context = context;
        audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        initVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        errorSubject = PublishSubject.create();
        setError(false);

    }

    public void setError(boolean error){
        this.error = error;
        errorSubject.onNext(error);
    }
    public boolean isError(){
        return error;
    }
    public Observable<Boolean> getObservable(){
        return errorSubject.hide();
    }

    public void play(String id, int volume){
        if(id == null || id.equals("")){
            setError(true);
            return ;
        }
        Uri uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build();
            audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setAudioAttributes(audioAttributes)
                    .build();

            int res = audioManager.requestAudioFocus(audioFocusRequest);
            if(res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                try{
                    mediaPlayer = MediaPlayer.create(context, uri);
                    mediaPlayer.start();
                }catch (Exception e){
                    Log.e(TAG, "instance/state error.", e);
                    setError(true);
                }
            }else{
                Log.d(TAG, "audio focus gain failed.");
                setError(true);
            }
        }else{
            afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    if(focusChange == AudioManager.AUDIOFOCUS_GAIN){
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                    }
                    else{
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, initVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                    }
                }
            };
            int res = audioManager.requestAudioFocus(afChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if(res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
                try{
                    mediaPlayer = MediaPlayer.create(context, uri);
                    mediaPlayer.start();
                }catch (Exception e){
                    Log.e(TAG, "instance/state error.", e);
                    setError(true);
                }
            }else{
                Log.d(TAG, "audio focus gain failed.");
                setError(true);
            }
        }

    }

    public void release(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, initVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
            if(audioFocusRequest != null){
                audioManager.abandonAudioFocusRequest(audioFocusRequest);
            }
        }else{
            if(afChangeListener != null){
                audioManager.abandonAudioFocus(afChangeListener);
            }
        }
        if(mediaPlayer != null){
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }



//    private AudioManager audioManager;
//    private YouTubeItemViewModel youTubeItemViewModel;
//    private MediaPlayer mediaPlayer;
//    private AudioFocusRequest audioFocusRequest;
//    private int initVolume;
//    private AudioManager.OnAudioFocusChangeListener listener = new AudioManager.OnAudioFocusChangeListener() {
//        @Override
//        public void onAudioFocusChange(int focusChange) {
//            if(focusChange == AudioManager.AUDIOFOCUS_GAIN){
//                //set volume, start playback
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, youTubeItemViewModel.item.getValue().getVolumeSize(), AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//            }else{
//                //set volume, stop & release
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, initVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//            }
//        }
//    };
//    public MusicPlayerController(Context context, YouTubeItemViewModel youTubeItemViewModel){
//        audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
//        initVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
//        this.youTubeItemViewModel = youTubeItemViewModel;
//        String strUri = youTubeItemViewModel.item.getValue().getMusicItem().getContentUri();
//        if(strUri != null && strUri.length() >0 ){
//            Uri uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, strUri);
//            mediaPlayer = MediaPlayer.create(context, uri);
//        }
//    }
//
//    public void play(){
//        if(mediaPlayer == null){
//            youTubeItemViewModel.setMusicPlayerError(true);
//            return ;
//        }
//        //focus 획득, 실행
//        int res = 0;
//        AudioAttributes attributes = new AudioAttributes.Builder()
//                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//                .setUsage(AudioAttributes.USAGE_MEDIA)
//                .build();
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
//            .setAudioAttributes(attributes)
//            .setOnAudioFocusChangeListener(listener)
//            .build();
//            res = audioManager.requestAudioFocus(audioFocusRequest);
//        }else{
//            res = audioManager.requestAudioFocus(listener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
//        }
//        if(res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
//            mediaPlayer.start();
//        }else if(res == AudioManager.AUDIOFOCUS_REQUEST_FAILED){
//            youTubeItemViewModel.setMusicPlayerError(true);
//        }else{
//            youTubeItemViewModel.setMusicPlayerError(true);
//        }
//    }
//
//    public void release(){
//        if(mediaPlayer != null){
//            mediaPlayer.release();
//            mediaPlayer = null;
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if(audioFocusRequest != null){
//                audioManager.abandonAudioFocusRequest(audioFocusRequest);
//            }
//        }else{
//            audioManager.abandonAudioFocus(listener);
//        }
//    }
}
