package com.test.alarm2uv2.view.add;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.test.alarm2uv2.R;
import com.test.alarm2uv2.databinding.ViewYoutubeAlertBinding;
import com.test.alarm2uv2.viewmodel.DetailViewModel;
import com.test.alarm2uv2.viewmodel.ItemViewModel;

public class YouTubeDialogFragment extends DialogFragment{

    private static final String TAG = "YouTubeDialogFragment.class";
    private YouTubeDialogListener listener;
    private Point displaySize;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        displaySize = new Point();
        WindowManager windowManager = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getSize(displaySize);
        //viewModel - activity의 viewModel 공유하는 방법
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider.AndroidViewModelFactory.getInstance(getActivity().getApplication());
        DetailViewModel viewModel = new ViewModelProvider(requireActivity(),factory).get(DetailViewModel.class);
        //databinding
        ViewYoutubeAlertBinding binding = DataBindingUtil.inflate(inflater, R.layout.view_youtube_alert, container, false);
        binding.setViewModel(viewModel);

        viewModel.openYouTubeApp.observe(this, aVoid -> listener.openYouTubeApp());
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = (int)(displaySize.x*0.8);
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(params);
        getDialog().getWindow().setGravity(Gravity.CENTER);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    public interface YouTubeDialogListener{
        void openYouTubeApp();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (YouTubeDialogListener)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+" must implements YouTubeDialogListener.");
        }
    }
}
