package com.test.alarm2uv2.view.add;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.test.alarm2uv2.R;
import com.test.alarm2uv2.adapter.MusicItemAdapter;
import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.databinding.ActivityAddBinding;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.model.resource.db.MusicItem;
import com.test.alarm2uv2.view.main.MainActivity;
import com.test.alarm2uv2.viewmodel.DetailViewModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class AddItemActivity extends AppCompatActivity implements YouTubeDialogFragment.YouTubeDialogListener, MusicItemAdapter.OnItemListener {

    private DetailViewModel viewModel;
    private YouTubeDialogFragment youTubeDialog;
    private MusicListDialogFragment musicListDialog;
    private final int RC_READ_EXTERNAL_STORAGE = 1000;
    private final String TAG = "AddItemActivity.class";
    private CompositeDisposable compositeDisposable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setting
        ActivityAddBinding activityAddBinding = DataBindingUtil.setContentView(this, R.layout.activity_add);
        setSupportActionBar(activityAddBinding.topAppBarAdd);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication());
        viewModel = new ViewModelProvider(AddItemActivity.this, factory).get(DetailViewModel.class);
        activityAddBinding.setViewModel(viewModel);
        activityAddBinding.setLifecycleOwner(AddItemActivity.this);


        //description
        activityAddBinding.edittextDescId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_DONE ){
                    viewModel.setDescription(v.getText().toString()== null?"":v.getText().toString());
                    activityAddBinding.edittextDesc.clearFocus();
                    Log.d(TAG, "done.");
                }
                return false;
            }
        });
        //intent
        int mode = getIntent().getIntExtra(CommonCode.FLAG, -1);
        viewModel.setMode(mode);
        viewModel.getMode().observe(AddItemActivity.this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                //binding - volume
                AudioManager audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
                if(integer == CommonCode.CREATE){
                    viewModel.setItem(new Item());
                    viewModel.setHour(Util.getCurrentHour());
                    viewModel.setMinute(Util.getCurrentMinute());
                }else{
                    Item item = getIntent().getParcelableExtra(CommonCode.ITEM);
                    viewModel.setItem(item);
                    viewModel.setHour(Util.getHour(item.getAlarmTime()));
                    viewModel.setMinute(Util.getMinute(item.getAlarmTime()));
                }
                viewModel.setMaxVolume(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
                viewModel.getItem().getValue().setVolumeSize(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

            }
        });
        //observer 리소스 해제
        compositeDisposable = new CompositeDisposable();
        //youtube
        viewModel.openYouTubeDialog.observe(this, aVoid -> {
            youTubeDialog = new YouTubeDialogFragment();
            youTubeDialog.show(getSupportFragmentManager(), "youtube_dialog");
        });

        viewModel.saveYouTubeInfo.observe(this, aVoid -> {
            youTubeDialog.dismiss();
            if( compositeDisposable.size() > 0 ){
                compositeDisposable.clear();
            }
            viewModel.callYouTubeInfo(viewModel.getItem().getValue().getYouTubeItem().getYouTubeUri()).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<String>() {
                        @Override
                        public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                            viewModel.setWaitState(View.VISIBLE);
                            compositeDisposable.add(d);
                            Log.d(TAG, "onSubscribe. (get YouTube Item) ");
                        }

                        @Override
                        public void onSuccess(@io.reactivex.annotations.NonNull String s) {
                            Document doc = Jsoup.parse(s);
                            String title = doc.select("meta[property=og:title]").attr("content");
                            String thumbnail = doc.select("meta[property=og:image]").attr("content");
                            viewModel.getItem().getValue().getYouTubeItem().setTitle(title);
                            viewModel.getItem().getValue().getYouTubeItem().setThumbnail(thumbnail);
                            viewModel.setWaitState(View.GONE);
                            Log.d(TAG, "onSuccess. (get YouTube Item) ");
                        }

                        @Override
                        public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                            viewModel.setWaitState(View.GONE);
                            Log.d(TAG, "onError. (get YouTube Item) ");
                        }
                    });
        });
//        //block 걸지 않는게 좋을 듯
//        itemViewModel.getWaitState().observe(this, integer -> {
//            if (integer == View.VISIBLE) {
//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//            } else {
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//            }
//        });
        //music
        viewModel.openMusicDialog.observe(this, aVoid -> {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                musicListDialog = new MusicListDialogFragment();
                musicListDialog.show(getSupportFragmentManager(), "music_dialog");
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_READ_EXTERNAL_STORAGE);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (requestCode == RC_READ_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(AddItemActivity.this, "접근을 허가하였습니다.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(AddItemActivity.this, "거부하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if(compositeDisposable.size() > 0){
            compositeDisposable.clear();
        }
        super.onDestroy();

    }

    @Override
    public void onItemClick(MusicItem musicItem) {
        viewModel.setMusicItem(musicItem);
        musicListDialog.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.save:
                viewModel.onClickSaveItem();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(CommonCode.ITEM, viewModel.getItem().getValue());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void openYouTubeApp() {
        try{
            Intent intent = getPackageManager().getLaunchIntentForPackage(CommonCode.YOUTUBE_PACKAGE_NAME);
            if(intent == null){
                throw new Exception();
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "유튜브 앱을 실행할 수 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

}
