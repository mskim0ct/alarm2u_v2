package com.test.alarm2uv2.view.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;

/********************************
 * YouTube Application 관련 클래스
 * - YouTube 서비스 실행을 위한 초기화
 * - YouTube 서비스 가능 여부
 * ( Android YouTube API 라이브러리에서 YouTubeApiServiceUtil 및 YouTubeInitializationResult 클래스를 이용한 클래스 )
 * ******************************/
public class YouTubeAppUtil{

    public interface CancelClickListener{
        void onCancelClick();
    }
    private CancelClickListener cancelClickListener;

    public YouTubeAppUtil(CancelClickListener listener){
        if(listener == null){
            Log.e(YouTubeAppUtil.class.getSimpleName(), "cancelClickListener must be implementation.");
        }else{
            cancelClickListener = listener;
        }
    }
//    public boolean isInstalledYouTubeApp(Context context){
//        List<ApplicationInfo> list = context.getPackageManager().getInstalledApplications(0);
//        for(ApplicationInfo appInfo : list){
//            if(appInfo.equals(YOUTUBE_PACKAGE_NAME)){
//                return true;
//            }
//        }
//        return false;
//    }
//    public Intent openYouTubeApp(Context context){
//        Intent intent = context.getPackageManager().getLaunchIntentForPackage(CommonCode.YOUTUBE_PACKAGE_NAME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        return intent;
//    }
    public YouTubeInitializationResult isYouTubeApiServiceAvailable(Context context){
        return YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context);
    }
    public boolean isUserRecoverableError(YouTubeInitializationResult result){
        return result.isUserRecoverableError();
    }
    public void showErrorDialog(YouTubeInitializationResult result, Activity activity, int requestCode){
        AlertDialog alertDialog = (AlertDialog)result.getErrorDialog(activity, requestCode);
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelClickListener.onCancelClick();
                dialog.cancel();
            }
        });
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLUE);
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
    }
}
