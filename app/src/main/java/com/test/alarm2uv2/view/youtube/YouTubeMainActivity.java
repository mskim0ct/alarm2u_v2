package com.test.alarm2uv2.view.youtube;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.common.alarm.AlarmController;
import com.test.alarm2uv2.Application;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.model.resource.db.MusicItem;
import com.test.alarm2uv2.model.resource.db.YouTubeItem;
import com.test.alarm2uv2.service.YouTubeMainService;
//import com.test.alarm2uv2.view.youtube.YouTubePlayerController;
import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.databinding.ActivityYoutubeBinding;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.viewmodel.YouTubeItemViewModel;

import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/***************************
 * 1. YouTube 영상 재생
 * 2. YouTube 영상 에러 -> 등록된 음악 재생
 * 3. 등록된 음악 재생 불가 -> 알람 불가 알림 띄우고 종료
 *
 * 4. YouTube || 음악 재생 -> snooze, 종료
 * 5. snooze -> snoozeAlarmTime 계산
 * 6. 종료 -> isRunFlag  False
 * ********************************/

//YouTubeBaseActivity 는 Activity를 상속받고 Activity는 Lifecycleowner 따위 없으므로..직접 구현해야 함 .. ㅎ
// -> 아니면.. YouTubeBaseActivity를 사용하지 않고 YouTube를 Fragment로 적용해서 그대로 AppCompatActivity를 사용할 수 있음
// 자료 : https://developer.android.com/topic/libraries/architecture/lifecycle?hl=ko
public class YouTubeMainActivity extends YouTubeBaseActivity implements LifecycleOwner {

    private static final String TAG = "YouTubeMainActivity.class";
    private ActivityYoutubeBinding binding;
    private LifecycleRegistry lifecycleRegistry;
    private YouTubeItemViewModel viewModel;
    private YouTubePlayerController youTubePlayerController;
    private MusicPlayerController musicPlayerController;
    private Vibrator vibrator;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        //lifecycle 등록
        lifecycleRegistry = new LifecycleRegistry(this);
        lifecycleRegistry.setCurrentState(Lifecycle.State.CREATED);
        //Data Binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_youtube);
        binding.setLifecycleOwner(this);
        //ViewModel
        ViewModelProvider.AndroidViewModelFactory factory = new ViewModelProvider.AndroidViewModelFactory(getApplication());
        viewModel = factory.create(YouTubeItemViewModel.class);
        binding.setYoutubeItemViewModel(viewModel);
        //Item
        Item item= getIntent().getParcelableExtra(CommonCode.ITEM);
//        Item item = new Item();
//        item.setAlarmTime(Util.getCurrentTime());
//        item.setRegistrationTime(Util.getCurrentTime());
//        item.setUpdateTime(Util.getCurrentTime());
//        item.setSnooze(0);
//        item.setVibrate(false);
//        item.setVolumeSize(7);
//        item.setRunFlag(true);
//        item.setDescription("");
//        item.setYouTubeItem(new YouTubeItem("https://youtu.be/YL3kBu099io","title","thumbnail"));
//        item.setMusicItem(new MusicItem("","",""));
        /////////////////////////////////////
        viewModel.setItem(item);
        //화면꺼짐 | 화면 잠김
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setTurnScreenOn(true);
            setShowWhenLocked(true);
            KeyguardManager keyguardManager = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);
            keyguardManager.requestDismissKeyguard(this, null);
        }else{
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        }
        viewModel.showActivity.setValue(true);
        // 영상/음악 실행 : YouTube & Music
        youTubePlayerController = new YouTubePlayerController(this);
        musicPlayerController = new MusicPlayerController(this);
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        viewModel.showActivity.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    //진동설정
                    vibrationSet(item.isVibrate());
                    //youtube play
                    binding.youtubeplayerviewYoutube.initialize(getString(R.string.developer_key), new YouTubePlayer.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                            youTubePlayerController.setYouTubePlayer(youTubePlayer);
                            youTubePlayerController.play(item.getYouTubeItem().getYouTubeUri(), item.getVolumeSize());
                        }

                        @Override
                        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                            youTubePlayerController.setError(true);
                            Log.d(TAG, "init error : " + youTubeInitializationResult.name());
                        }
                    });
                }
            }
        });
        //youtube play error
        compositeDisposable.add(youTubePlayerController.getObservable()
                .subscribe(aBoolean->{
                        Log.d(TAG, "aBoolean(youtube play error) : "+aBoolean);
                        if(aBoolean){
                            //youtube 종료
                            youTubePlayerController.release();
                            //music play
                            musicPlayerController.play(item.getMusicItem().getContentUri(), item.getVolumeSize());
                        }
                    }, Throwable::printStackTrace));
        //music player error
        compositeDisposable.add(musicPlayerController.getObservable()
                            .subscribe(aBoolean -> {
                                Log.d(TAG, "aBoolean(music play error) : "+aBoolean);
                                if(aBoolean){
                                    musicPlayerController.release();
                                    notifyAlarmErrorReason();
                                    finish();
                                }
                            }, Throwable::printStackTrace));

        //activity 종료
        viewModel.finish.observe(this, new Observer<Void>() {
            @Override
            public void onChanged(Void aVoid) {
                compositeDisposable.add(Completable.fromCallable(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        viewModel.updateItem(viewModel.item.getValue());
                        AlarmController alarmController = new AlarmController(YouTubeMainActivity.this);
                        alarmController.createAlarm(viewModel.getItems());
                        return null;
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(()->{
                            finish();
                        }, Throwable::printStackTrace));
            }
        });
        //서비스 종료
        if(!isStopService()){
            stopService(new Intent(this, YouTubeMainService.class));
        }
    }

    private boolean isStopService(){
        ActivityManager activityManager = (ActivityManager)getSystemService(Activity.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service :  activityManager.getRunningServices(Integer.MAX_VALUE)){
            if(service.service.getClassName().equals("YouTubeMainService")){
                return true;
            }
        }
        return false;
    }
    private void vibrationSet(boolean val){
        if(!val){
            return ;
        }
        if(!vibrator.hasVibrator()){
            return ;
        }
        vibrator.vibrate(new long[]{1000L, 3000L, 1000L, 1000L}, 0);
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            VibrationEffect vibrationEffect = VibrationEffect.createWaveform(new long[]{1000L, 3000L, 1000L, 1000L}, new int[]{0, VibrationEffect.DEFAULT_AMPLITUDE,0,VibrationEffect.DEFAULT_AMPLITUDE}, 0);
//            vibrator.vibrate(vibrationEffect, new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
//            .setUsage(AudioAttributes.USAGE_MEDIA).build());
//        }else{
//            vibrator.vibrate(new long[]{1000L, 3000L, 1000L, 1000L}, 0);
//        }
        Log.d(TAG, "vibrator work");
    }
    private void notifyAlarmErrorReason(){
       NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Application.NOTIFICATION_ALARM_ERROR_CHANNEL_ID)
               .setSmallIcon(R.drawable.ic_noti_alarm_error)
               .setLargeIcon(createBitmap(R.drawable.ic_launcher_foreground))
               .setContentTitle("알람 종료")
               .setContentText("유튜브 영상 및 설정한 음악을 실행할 수 없어 알람을 종료합니다.")
               .setStyle(new NotificationCompat.BigTextStyle().bigText("유튜브 영상 및 설정한 음악을 실행할 수 없어 알람을 종료합니다. snooze 알람일 경우 이후 알람이 발생하지 않습니다."))
               .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
               .setAutoCancel(true);
        NotificationManagerCompat.from(this).notify(CommonCode.NOTIFICATION_ALARM_ERROR_ID, builder.build());
    }
    private Bitmap createBitmap(int drawableId){
        Drawable drawable =  ContextCompat.getDrawable(this, drawableId);
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0,0,bitmap.getWidth(), bitmap.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        lifecycleRegistry.setCurrentState(Lifecycle.State.STARTED);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        lifecycleRegistry.setCurrentState(Lifecycle.State.RESUMED);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        youTubePlayerController.release();
        musicPlayerController.release();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        vibrator.cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        compositeDisposable.clear();
        lifecycleRegistry.setCurrentState(Lifecycle.State.DESTROYED);

    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }
}
