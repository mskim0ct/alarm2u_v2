package com.test.alarm2uv2.view.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.common.alarm.AlarmController;
import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.adapter.ItemAdapter;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.databinding.ActivityMainBinding;

import com.test.alarm2uv2.helper.SwipeItemTouchHelperCallback;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.view.add.AddItemActivity;
import com.test.alarm2uv2.viewmodel.ItemViewModel;

import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

//TODO - recyclerview item databinding 날짜 변경되는 거 확인하기
//TODO - RecyclerView 바깥 영역을 선택할 경우, SwipeItemTouchHelperCallback 내의 delete 대기 영역에 있는 item 삭제하는 부분 분리하기
public class MainActivity extends AppCompatActivity implements SwipeItemTouchHelperCallback.EventListener{
    private static final String TAG = "MainActivity.class";
    private ItemViewModel itemViewModel;
    private final int REQUEST_CODE_ALARM = 1;
    private final int REQUEST_CODE_YOUTUBE = 2;
    private final int REQUEST_CODE_DRAW_OVERLAYS_PERMISSION = 3;

    private ActivityMainBinding activityMainBinding;
    private ItemAdapter itemAdapter;
    private SwipeItemTouchHelperCallback callback;
    //알람 추가,수정,삭제에 대한 리소스 해제
    private CompositeDisposable compositeDisposable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(activityMainBinding.topAppBar);
        activityMainBinding.setLifecycleOwner(this);
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication());
        itemViewModel = new ViewModelProvider(MainActivity.this, factory).get(ItemViewModel.class);
        activityMainBinding.setViewModel(itemViewModel);
        compositeDisposable = new CompositeDisposable();
        //RecyclerView Setting
        itemAdapter = new ItemAdapter(new ItemAdapter.SwitchListener() {
            @Override
            public void onChanged(Item item, boolean user) {
                if(!user){
                    return ;
                }
                Log.d(TAG, "Switch 로 인해 변경되었습니다. - value : "+item.isRunFlag());
                if(!item.isRunFlag()){
                    item.setAlarmTime(item.getInitAlarmTime());
                    item.setSnooze(false);
                }else{
                    long alarmTime;
                    if(Util.isOnceAlarm(item.getDays())){
                        alarmTime = Util.nextAlarmTimeOnce(item.getInitAlarmTime());
                    }else{
                        alarmTime= Util.nextAlarmTimeRepeat(item.getInitAlarmTime(), item.getDays());
                    }
                    item.setInitAlarmTime(alarmTime);
                    item.setAlarmTime(alarmTime);
                    item.setSnooze(false);
                }
                compositeDisposable.add(updateItem(item));
            }
        });
        callback = new SwipeItemTouchHelperCallback(activityMainBinding.recyclerviewItem, this);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(activityMainBinding.recyclerviewItem);
        activityMainBinding.recyclerviewItem.setAdapter(itemAdapter);
        //알람 등록
        AlarmController alarmController = new AlarmController(getApplicationContext());
        //데이터 변경사항(LiveData)
        activityMainBinding.getViewModel().getAllItems().observe(this, items ->{
                itemAdapter.submitList(items);
                alarmController.createAlarm(items);
        });
        //알람 생성
        itemViewModel.createBtn.observe(this, aVoid->{
            itemViewModel.setMode(CommonCode.CREATE);
            Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
            intent.putExtra(CommonCode.FLAG, CommonCode.CREATE);
            startActivityForResult(intent, REQUEST_CODE_ALARM);
        });
        //알람 수정
        itemViewModel.detailBtn.observe(this, aVoid -> {
            itemViewModel.setMode(CommonCode.UPDATE);
            Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
            intent.putExtra(CommonCode.FLAG, CommonCode.UPDATE);
            intent.putExtra(CommonCode.ITEM, itemViewModel.getItem().getValue());
            startActivityForResult(intent, REQUEST_CODE_ALARM);
        });
        //YouTube App 확인 (YouTube App 설치 여부, version 문제 등)
        YouTubeAppUtil youTubeAppUtil = new YouTubeAppUtil(new YouTubeAppUtil.CancelClickListener() {
            @Override
            public void onCancelClick() {
                showYouTubeInitError();
            }
        });
        YouTubeInitializationResult result = youTubeAppUtil.isYouTubeApiServiceAvailable(this);
        if(youTubeAppUtil.isUserRecoverableError(result)){
            youTubeAppUtil.showErrorDialog(result, this, REQUEST_CODE_YOUTUBE);
        }
        //SYSTEM_ALERT_WINDOW permission 수락 여부 확인
        itemViewModel.callPermissionOverlay.observe(this, new Observer<Void>() {
            @Override
            public void onChanged(Void aVoid) {
                requestDrawOverlaysPermission();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Settings.canDrawOverlays(this.getApplicationContext())){
            itemViewModel.setPermissionOverlayBanner(View.VISIBLE);
        }else{
            itemViewModel.setPermissionOverlayBanner(View.GONE);
        }
    }

    private void requestDrawOverlaysPermission(){
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:"+getPackageName()));
        startActivityForResult(intent, REQUEST_CODE_DRAW_OVERLAYS_PERMISSION);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_ALARM){
            if (resultCode == RESULT_OK) {
                Item item = data.getParcelableExtra(CommonCode.ITEM);
                if(itemViewModel.getMode().getValue() == CommonCode.CREATE){
                    compositeDisposable.add(insertItem(item));
                }else{
                    compositeDisposable.add(updateItem(item));
                }
            }
        }else if(requestCode == REQUEST_CODE_YOUTUBE){
            if(resultCode != RESULT_OK){
                showYouTubeInitError();
            }
        }else if(requestCode == REQUEST_CODE_DRAW_OVERLAYS_PERMISSION){
            if(resultCode == RESULT_OK){
                itemViewModel.setPermissionOverlayBanner(View.GONE);
                Toast.makeText(getApplicationContext(), "'다른 앱 위에 화면 띄우기'권한을 허용하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showYouTubeInitError(){
        Toast.makeText(getApplicationContext(), "YouTube 영상이 재생되지 않을 수 있습니다.", Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.menu_delete_id){
            compositeDisposable.add(deleteItems());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickDetailBtn(int pos) {
        itemViewModel.setItem(itemAdapter.getItem(pos));
        itemViewModel.onClickDetailView();
    }

    @Override
    public void onClickDelete(int pos) {
        Item item = itemAdapter.getItem(pos);
        compositeDisposable.add(deleteItem(item));
    }

    //RecyclerView의 바깥 영역에서 터치가 발생했을 때, delete  대기 알람이 있을 경우, delete 대기 알람을 취소하기 위한 부분
    // 터치 영역이 RecyclerView 내에 있다면, 해당 터치 무시 ( RecyclerView 영역일 경우는 SwipeItemTouchHelperCallback에서 처리함)
    // 터치 영영이 RecyclerView 밖에 영역이라면, item 을 delete 대기 영역에서 취소시킴.
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "dispatchTouchEvent");
        float x = ev.getX();
        float y = ev.getY();
        if(!findRecyclerView(activityMainBinding.recyclerviewItem, x, y)){ //터치 영역이 RecyclerView 영역인지 확인하는 함수
            callback.recoverItems();
        }
        return super.dispatchTouchEvent(ev);
    }

    private boolean findRecyclerView(View view, float x, float y){
        return x > view.getLeft() && x < view.getX() + view.getRight() && y > view.getTop() && y < view.getBottom();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private Disposable insertItem(Item item){
        return Completable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                itemViewModel.insertItem(item);
                return null;
            }
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(()->{
                Toast.makeText(getApplicationContext(), "알람이 등록되었습니다.", Toast.LENGTH_SHORT).show();
            }, Throwable->{
                Log.e(TAG, "알람 등록 실패", Throwable);
                Toast.makeText(getApplicationContext(), "알람 등록을 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        );
    }
    private Disposable updateItem(Item item){
        return Completable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                itemViewModel.updateItem(item);
                return null;
            }
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(()->{
                Toast.makeText(getApplicationContext(), "알람이 수정되었습니다.", Toast.LENGTH_SHORT).show();
            }, Throwable->{
                Log.e(TAG, "알람 수정 실패", Throwable);
                Toast.makeText(getApplicationContext(), "알람 수정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        );
    }
    private Disposable deleteItem(Item item){
        return Completable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                itemViewModel.deleteItem(item);
                return null;
            }
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(()->{
                Toast.makeText(getApplicationContext(), "알람을 삭제하였습니다..", Toast.LENGTH_SHORT).show();
            },Throwable->{
                Log.e(TAG, "알람 삭제 실패", Throwable);
                Toast.makeText(getApplicationContext(), "알람 삭제에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            });
    }
    private Disposable deleteItems(){
        return Completable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                itemViewModel.deleteItems();
                return null;
            }
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(()->{
                Toast.makeText(getApplicationContext(), "모든 알람을 삭제하였습니다.", Toast.LENGTH_SHORT).show();
            },
            Throwable->{
                Log.e(TAG, "모든 알람 삭제 실패.", Throwable);
                Toast.makeText(getApplicationContext(), "모든 알람을 삭제하는데 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        );
    }
}
