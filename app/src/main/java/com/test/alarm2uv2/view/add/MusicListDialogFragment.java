package com.test.alarm2uv2.view.add;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.test.alarm2uv2.adapter.MusicItemAdapter;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.databinding.ViewMusicAlertBinding;
import com.test.alarm2uv2.model.resource.db.MusicItem;
import com.test.alarm2uv2.viewmodel.DetailViewModel;

import java.util.List;

public class MusicListDialogFragment extends DialogFragment{

    public static final String TAG = MusicListDialogFragment.class.getSimpleName();

    private Point displaySize;
    private DetailViewModel viewModel;
    private MusicItemAdapter.OnItemListener listener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        displaySize = new Point();
        WindowManager windowmanager = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getSize(displaySize);
        //databinding
        ViewMusicAlertBinding binding = DataBindingUtil.inflate(inflater, R.layout.view_music_alert, container,false);
        //ViewModel 등록
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider.AndroidViewModelFactory.getInstance(getActivity().getApplication());
        viewModel = new ViewModelProvider(requireActivity(), factory).get(DetailViewModel.class);
        binding.setViewModel(viewModel);
        //recyclerview
        List<MusicItem> musicItems = viewModel.callMusicList();
        viewModel.setMusicItems(musicItems);
        MusicItemAdapter musicItemAdapter = new MusicItemAdapter(listener);
        musicItemAdapter.submitList(musicItems);
        binding.recyclerviewMusic.setAdapter(musicItemAdapter);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
/*        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;*/
        params.width = (int)(displaySize.x*0.8);
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(params);
        getDialog().getWindow().setGravity(Gravity.CENTER);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView() 호출");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (MusicItemAdapter.OnItemListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+"must implements OnClickListener.");
        }
    }
}
