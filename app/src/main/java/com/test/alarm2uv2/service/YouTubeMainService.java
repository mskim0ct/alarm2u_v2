package com.test.alarm2uv2.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.test.alarm2uv2.Application;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.common.alarm.AlarmController;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.model.ItemRepository;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.view.youtube.YouTubeMainActivity;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**************************
 * Service 를 실행하여 YouTubeMainActivity를 띄움
 * - 알람을 계산하고 Activity 실행 ( Permission - SYSTEM_ALERT_WINDOW 있어야 함 )
 * - SYSTEM_ALERT_WINDOW : MainActivity에서 사용자에게 권한 허락을 구함
 * - Permission OK  -> Activity 띄우기
 * - Permission Fail -> Notification 띄우기 ??
 * *************************/
public class YouTubeMainService extends Service {
    private final String TAG = "YouTubeMainService.class";
    private ItemRepository itemRepository;
    private CompositeDisposable compositeDisposable;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        compositeDisposable = new CompositeDisposable();
        //SDK 26 이상일 때, startForegroundService 시, Notification 추가
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = new NotificationCompat.Builder(this, Application.NOTIFICATION_ALARM_SERVICE_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_small_alarm)
                    .setContentText("")
                    .setContentTitle("유튜브 알람 서비스 실행").build();
            startForeground(1, notification);
        }
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        //알람 계산
        Log.i(TAG, ""+startId);

        itemRepository = new ItemRepository(getApplication());
        compositeDisposable.add(Completable.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                List<Item> items = itemRepository.getItems();
                calcItems(items);
                for(Item item : items){
                    itemRepository.updateItem(item);
                }
                AlarmController alarmController = new AlarmController(YouTubeMainService.this);
                alarmController.createAlarm(items);
                return null;
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(()->{
                    Log.d(TAG,"onCompleted.");
                    startYouTubeActivity(intent);
                })
                .doOnTerminate(()->{
                    Log.d(TAG, "stop service.");
                    stopSelf(startId);
                })
                .subscribe());

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
        Log.d(TAG,"onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startYouTubeActivity(Intent intent){
        Intent youtubeIntent = new Intent(YouTubeMainService.this, YouTubeMainActivity.class);
        youtubeIntent.putExtras(intent.getExtras());
        youtubeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(youtubeIntent);
    }

    private void calcItems(List<Item> items){
        for(Item item : items){
            if(item.getAlarmTime() <= Util.getCurrentTime()){
                if(Util.isOnceAlarm(item.getDays())){
                    item.setSnooze(false);
                    item.setRunFlag(false);
                    item.setAlarmTime(item.getInitAlarmTime());
                }else{
                    item.setSnooze(false);
                    long alarmTime = Util.nextAlarmTimeRepeat(item.getInitAlarmTime(), item.getDays());
                    item.setInitAlarmTime(alarmTime);
                    item.setAlarmTime(alarmTime);
                }
            }
        }
    }

}
