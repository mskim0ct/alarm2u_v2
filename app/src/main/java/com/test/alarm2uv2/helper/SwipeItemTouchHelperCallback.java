package com.test.alarm2uv2.helper;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.adapter.ItemAdapter;
import java.util.LinkedList;
import java.util.Queue;
/***********************************************************************
 * MainActivity 에 RecyclerView Item 에 대한 Swipe 및 Touch Handling 클래스
 * - Swipe : DELETE 표시하기
 * - Touch : DELETE 표시 : 내부 - 삭제
 *           DELETE 표시 : 외부 - recovery
 *           DELETE 표시X : DETAIL 화면으로 전송
 * **********************************************************************/
public class SwipeItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private static final String TAG = SwipeItemTouchHelperCallback.class.getSimpleName();

    public interface EventListener{
        void onClickDetailBtn(int pos);
        void onClickDelete(int pos);
    }
    private EventListener eventListener;
    private final Queue<Integer> posBuffer = new LinkedList<>();
    private final RecyclerView recyclerView;

    private int sX;
    private int sY;
    private long sTime;
    private final long CLICK_DURATION = 1500;
    private final RecyclerView.OnItemTouchListener onItemTouchListener = new RecyclerView.OnItemTouchListener() {

        @Override
        public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
            Log.d(TAG,"onInterceptTouchEvent");
            //해당하는 위치의 view가 switch 일때 return
                if(findChildView(rv, e.getRawX(), e.getRawY())){
                    return false;
                }
                switch(e.getAction()){
                    case MotionEvent.ACTION_DOWN:
                    sX = (int)e.getX();
                    sY = (int)e.getY();
                    sTime = System.currentTimeMillis();
                    break;
                    case MotionEvent.ACTION_UP:
                        int dX = (int)e.getX();
                        int dY = (int)e.getY();
                        long currentTime = System.currentTimeMillis();
                        if(sX > dX){ //swipe
                            break ;
                        }
                        if(currentTime - sTime < CLICK_DURATION){
                            //togo addActivity
                            if(posBuffer.isEmpty()){
                                View childView = rv.findChildViewUnder(dX, dY);
                                if(childView == null){
                                    break;
                                }
                                RecyclerView.ViewHolder viewHolder= rv.findContainingViewHolder(childView);
                                if(viewHolder instanceof ItemAdapter.ItemViewHolder){
                                    int pos = rv.getChildAdapterPosition(childView);
                                    eventListener.onClickDetailBtn(pos);
                                }
                                break;
                            }
                            while(posBuffer.size() > 1){
                                int position = posBuffer.poll();
                                recoverItem(position);
                            }
                            int swipePosition = posBuffer.poll();
                            RecyclerView.ViewHolder viewHolder = rv.findViewHolderForAdapterPosition(swipePosition);
                            View swipeView = viewHolder.itemView;
                            if(dX > swipeView.getLeft() && dX < swipeView.getRight() &&
                                    dY > swipeView.getTop() && dY < swipeView.getBottom()){
                                //delete
                                eventListener.onClickDelete(swipePosition);
                            }else{
                                //원상태
                                recoverItem(swipePosition);
                            }
                        }
                }
                return false;
        }

        @Override
        public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            Log.d(TAG, "onRequestDisallowInterceptTouchEvent");
        }
    };


    private boolean findChildView(View v, float x, float y) {
        boolean result = false;
        if(!(v instanceof ViewGroup)){
            if(v instanceof SwitchMaterial){
                Rect r = new Rect();
                v.getGlobalVisibleRect(r);
                if(x > r.left && x < r.right
                        && y > r.top && y < r.bottom){
                    result = true;
                }
            }
        }else{
            for(int i = 0 ; i < ((ViewGroup)v).getChildCount() ; i++){
                boolean tmp = findChildView(((ViewGroup)v).getChildAt(i), x, y);
                if(tmp){
                    return true;
                }
            }
        }
        return result;
    }

    public void recoverItems(){
        while(!posBuffer.isEmpty()){
            int posIndx = posBuffer.poll();
            recoverItem(posIndx);
        }
    }
    public void recoverItem(int pos){
        recyclerView.getAdapter().notifyItemChanged(pos);
    }

    @SuppressLint("ClickableViewAccessibility")
    public SwipeItemTouchHelperCallback(RecyclerView recyclerView, EventListener eventListener){
        this.recyclerView = recyclerView;
        setEventListener(eventListener);
        recyclerView.addOnItemTouchListener(onItemTouchListener);
    }

    public void setEventListener(EventListener eventListener){
        this.eventListener = eventListener;
    }
    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(0, ItemTouchHelper.LEFT);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        Log.d(TAG, "onSwiped 호출");
        recoverItems();
        int pos = viewHolder.getAdapterPosition();
        posBuffer.add(pos);
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
            if(dX < 0){
                float translationX = dX;
                drawButton(c, viewHolder.itemView, translationX);
            }
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
    public void drawButton(Canvas c, View itemView, float translationX){
        int buttonWidth = Math.min((-1)*(int)translationX, itemView.getRight()-itemView.getLeft());
        int left = itemView.getRight()-buttonWidth;
        int right = itemView.getRight();
        Rect rect = new Rect();
        rect.set(left, itemView.getTop(), right, itemView.getBottom());
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        c.drawRect(rect, paint);
        /*canvas size & background 설정 끝*/

        /*drawable to bitmap*/
        Drawable drawable = ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_delete);
        if(drawable != null){
            Bitmap bitmap = convertToBitmap(drawable);
            c.drawBitmap(bitmap, rect.left+(float)(rect.right-rect.left-bitmap.getWidth())/2, rect.top+(float)(rect.bottom-rect.top-bitmap.getHeight())/2, null);
        }
    }
    public Bitmap convertToBitmap(Drawable drawable){
        /*vector drawable 검정색으로 변경*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable.setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.BLACK, BlendModeCompat.SRC_IN));
        }else{
            drawable.setColorFilter(new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN));
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0,0,bitmap.getWidth(), bitmap.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
