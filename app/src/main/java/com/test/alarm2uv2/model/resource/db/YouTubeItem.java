package com.test.alarm2uv2.model.resource.db;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.test.alarm2uv2.BR;

import java.util.Objects;

@Entity(tableName = "youtube_table")
public class YouTubeItem extends BaseObservable implements Parcelable {

    String youTubeUri;
    @ColumnInfo(name = "youtube_title")
    String title;
    String thumbnail;

    public YouTubeItem(){}
    @Ignore
    public YouTubeItem(String youtubeUri, String title, String thumbnail){
        this.youTubeUri = youtubeUri;
        this.title = title;
        this.thumbnail = thumbnail;
    }

    @Ignore
    protected YouTubeItem(Parcel in) {
        youTubeUri = in.readString();
        title = in.readString();
        thumbnail = in.readString();
    }

    public static final Creator<YouTubeItem> CREATOR = new Creator<YouTubeItem>() {
        @Override
        public YouTubeItem createFromParcel(Parcel in) {
            return new YouTubeItem(in);
        }

        @Override
        public YouTubeItem[] newArray(int size) {
            return new YouTubeItem[size];
        }
    };

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }
    @Bindable
    public String getYouTubeUri() {
        return youTubeUri;
    }

    public void setYouTubeUri(String youTubeUri) {
        this.youTubeUri = youTubeUri;
        notifyPropertyChanged(BR.youTubeUri);
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(youTubeUri);
        dest.writeString(title);
        dest.writeString(thumbnail);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        YouTubeItem youTubeItem = (YouTubeItem) obj;
        return Objects.equals(youTubeUri, youTubeItem.youTubeUri) &&
                Objects.equals(title, youTubeItem.title) &&
                Objects.equals(thumbnail, youTubeItem.thumbnail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(youTubeUri, title,thumbnail);
    }
}
