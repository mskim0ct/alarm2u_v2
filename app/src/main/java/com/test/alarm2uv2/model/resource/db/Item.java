package com.test.alarm2uv2.model.resource.db;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.test.alarm2uv2.BR;

import java.util.Arrays;
import java.util.Objects;

@Entity
public class Item extends BaseObservable implements Parcelable{

    @PrimaryKey(autoGenerate = true)
    private int id;
    private long initAlarmTime; //처음 설정한 알람 시간
    private long alarmTime;  //설정한 알람에 대한 시간
    private long registrationTime; // 알람을 등록했을 때의 현재 시간
    private long updateTime; // 알람 수정했을 때의 현재 시간
    private boolean runFlag;   //switch 설정에 따른 알람 실행 여부 확인

    private int[] days; // 0 : 일, 1: 월, ... 6 : 토
    //private int snooze; //snooze 선택 횟수
    private boolean snooze;
    //private int repeatTime; //반복 5,10,30, 선택 안하면 0

    private boolean louder; //소리 점점 크게
    private boolean vibrate; //진동 여부
    private int volumeSize;
    private String description;
    @Embedded
    private YouTubeItem youTubeItem;
    @Embedded
    private MusicItem musicItem;

    public Item(){
        youTubeItem = new YouTubeItem("","","");
        musicItem = new MusicItem("","","");
        days = new int[7];
        description = "";
    }

    protected Item(Parcel in) {
        id = in.readInt();
        initAlarmTime = in.readLong();
        alarmTime = in.readLong();
        registrationTime = in.readLong();
        updateTime = in.readLong();
        runFlag = in.readByte() != 0;
        days = in.createIntArray();
        snooze = in.readByte() != 0;
        louder = in.readByte() != 0;
        vibrate = in.readByte() != 0;
        volumeSize = in.readInt();
        description = in.readString();
        youTubeItem = in.readParcelable(YouTubeItem.class.getClassLoader());
        musicItem = in.readParcelable(MusicItem.class.getClassLoader());
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public long getInitAlarmTime() {
        return initAlarmTime;
    }

    public void setInitAlarmTime(long initAlarmTime) {
        this.initAlarmTime = initAlarmTime;
    }

    public long getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(long alarmTime) {
        this.alarmTime = alarmTime;
    }

    public long getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(long registrationTime) {
        this.registrationTime = registrationTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isRunFlag() {
        return runFlag;
    }

    public void setRunFlag(boolean runFlag) {
        this.runFlag = runFlag;
    }

    public boolean isSnooze() {
        return snooze;
    }

    public void setSnooze(boolean snooze) {
        this.snooze = snooze;
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description == null){
            this.description = "";
        }
        this.description = description;
    }

    @Bindable
    public int[] getDays() {
        return days;
    }
    public void setDays(int[] days) {
        this.days = days;
        notifyPropertyChanged(BR.days);
    }

    public int getVolumeSize() {
        return volumeSize;
    }

    public void setVolumeSize(int volumeSize) {
        this.volumeSize = volumeSize;
    }


    @NonNull
    public YouTubeItem getYouTubeItem() {
        return youTubeItem;
    }
    public void setYouTubeItem(@NonNull YouTubeItem youTubeItem) {
        this.youTubeItem = youTubeItem;
    }

    @NonNull
    public MusicItem getMusicItem() {
        return musicItem;
    }

    public void setMusicItem(MusicItem musicItem) {
        this.musicItem = musicItem;
    }

    public boolean isLouder() {
        return louder;
    }

    public void setLouder(boolean louder) {
        this.louder = louder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                initAlarmTime == item.initAlarmTime &&
                alarmTime == item.alarmTime &&
                registrationTime == item.registrationTime &&
                updateTime == item.updateTime &&
                runFlag == item.runFlag &&
                snooze == item.snooze &&
                louder == item.louder &&
                vibrate == item.vibrate &&
                volumeSize == item.volumeSize &&
                Arrays.equals(days, item.days) &&
                description.equals(item.description) &&
                youTubeItem.equals(item.youTubeItem) &&
                musicItem.equals(item.musicItem);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, initAlarmTime, alarmTime, registrationTime, updateTime, runFlag, snooze, louder, vibrate, volumeSize, description, youTubeItem, musicItem);
        result = 31 * result + Arrays.hashCode(days);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeLong(initAlarmTime);
        dest.writeLong(alarmTime);
        dest.writeLong(registrationTime);
        dest.writeLong(updateTime);
        dest.writeByte((byte) (runFlag ? 1 : 0));
        dest.writeIntArray(days);
        dest.writeByte((byte) (snooze ? 1 : 0));
        dest.writeByte((byte) (louder ? 1 : 0));
        dest.writeByte((byte) (vibrate ? 1 : 0));
        dest.writeInt(volumeSize);
        dest.writeString(description);
        dest.writeParcelable(youTubeItem, flags);
        dest.writeParcelable(musicItem, flags);
    }
}
