package com.test.alarm2uv2.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.test.alarm2uv2.model.resource.db.Item;
import java.util.List;

@Dao
public interface ItemDAO {
    @Insert
    void insert(Item item);
    @Update
    void update(Item item);
    @Delete
    void delete(Item item);
    @Query("delete from Item")
    void deleteAllItems();
    @Query("select * from Item order by alarmTime asc")
    List<Item> getItems();
    @Query("select * from Item order by registrationTime desc, updateTime desc")
    LiveData<List<Item>> getLiveDataAllItems();
}
