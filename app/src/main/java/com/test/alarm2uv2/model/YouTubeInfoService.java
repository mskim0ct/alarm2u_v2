package com.test.alarm2uv2.model;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface YouTubeInfoService {
    @GET("{id}")
    Single<String> getYouTubeInfo(@Path("id") String id);
}
