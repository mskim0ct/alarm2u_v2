package com.test.alarm2uv2.model;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.model.resource.db.MusicItem;

import java.util.List;
import io.reactivex.Single;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ItemRepository {
    private static final String TAG = "ItemRepository.class";
    private final ItemDAO itemDAO;
    private final LiveData<List<Item>> items;
    private final MusicContentResolver musicResolver;
    private final YouTubeInfoService youTubeInfoService;

    public ItemRepository(Application application){
        ItemDatabase itemDatabase = ItemDatabase.getInstance(application.getApplicationContext());
        itemDAO = itemDatabase.itemDao();
        items = itemDAO.getLiveDataAllItems();
        musicResolver = new MusicContentResolver(application);
        //get youtube info using retrofit2
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://youtu.be")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        youTubeInfoService = retrofit.create(YouTubeInfoService.class);
    }
    public Single<String> callYouTubeInfo(String uri){
        return youTubeInfoService.getYouTubeInfo(uri);
    }

    public void insertItem(Item item){
        itemDAO.insert(item);
    }
    public void updateItem(Item item){
        itemDAO.update(item);
    }
    public List<Item> getItems(){
        return itemDAO.getItems();
    }
    public void deleteItem(Item item){
        itemDAO.delete(item);
    }
    public void deleteItems(){
        itemDAO.deleteAllItems();
    }

    public LiveData<List<Item>> getLiveDataAllItems(){
        return items;
    }

    public List<MusicItem> getMusicItems(){
        return musicResolver.getMusicItems();
    }

}
