package com.test.alarm2uv2.model;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.test.alarm2uv2.converter.db.Converters;
import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.model.resource.db.MusicItem;
import com.test.alarm2uv2.model.resource.db.YouTubeItem;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@TypeConverters({Converters.class})
@Database(entities = {Item.class}, version=1, exportSchema = false)
public abstract class ItemDatabase extends RoomDatabase {
    public abstract ItemDAO itemDao();
    private static ItemDatabase instance;
    public static synchronized ItemDatabase getInstance(Context context){
        if(instance == null){
                instance = Room.databaseBuilder(context, ItemDatabase.class, "item_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(ITEM_CALLBACK).build();
        }
        return instance;
    }

    private static final ItemDatabase.Callback ITEM_CALLBACK = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            //new FirstThread(instance).start();
        }
    };

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {
    }
}
