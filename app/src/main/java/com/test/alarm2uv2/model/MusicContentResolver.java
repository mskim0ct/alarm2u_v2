package com.test.alarm2uv2.model;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import androidx.annotation.MainThread;

import com.test.alarm2uv2.model.resource.db.MusicItem;

import java.util.ArrayList;
import java.util.List;

public class MusicContentResolver {
    private final Context context;
    private final Uri uri;
    private final String[] projection;
    private List<MusicItem> musicItems;

    public MusicContentResolver(Application application){
        context = application.getApplicationContext();
        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        projection = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST};
    }

    public List<MusicItem> getMusicItems(){
        musicItems = new ArrayList<>();
        String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE+"=?";
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension("mp3");
        Cursor cursor = context.getContentResolver().query(uri, projection, selectionMimeType, new String[]{mimeType}, MediaStore.Audio.Media.TITLE+" ASC" );
        while(cursor.moveToNext()) {
            int indx  = cursor.getColumnIndex(projection[0]);
            String id = cursor.getString(indx);
            indx = cursor.getColumnIndex(projection[1]);
            String title = cursor.getString(indx);
            indx = cursor.getColumnIndex(projection[2]);
            String artist = cursor.getString(indx);
            musicItems.add(new MusicItem(id, title, artist));
        }
        return musicItems;
    }

}
