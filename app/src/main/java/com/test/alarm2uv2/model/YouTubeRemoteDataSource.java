package com.test.alarm2uv2.model;

import android.graphics.Bitmap;
import android.util.Log;

import com.test.alarm2uv2.model.resource.db.YouTubeItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/*********
 * title, thumbnail 같이 가져오기
 * *********/
public class YouTubeRemoteDataSource {

    private final String uri;
    public YouTubeRemoteDataSource(String uri){
        this.uri = uri;
    }
    public YouTubeItem getYouTubeInfo(){
        YouTubeItem youtubeItem = null;
        String title = "";
        String thumbnail = "";
        try {
            Document doc = Jsoup.connect(uri).get();
            Elements elements = doc.getElementsByTag("meta");
            boolean findTitle = false;
            boolean findThumbnail = false;
            for(Element element : elements){
                if(element.attr("name").equals("title")){
                    title = element.attr("content");
                    findTitle = true;
                }
                if(element.attr("property").equals("og:image")){
                    thumbnail = element.attr("content");
                    findThumbnail = true;
                }
                if(findTitle && findThumbnail){
                    break;
                }
            }
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), e.getStackTrace()[0].toString());
        }finally {
            youtubeItem = new YouTubeItem(uri, title, thumbnail);
        }
        return youtubeItem;
    }
//    public String[] getYouTubeInfo(){
//        String title = "";
//        String thumbnail = "";
//        try {
//            Document doc = Jsoup.connect(uri).get();
//            Elements elements = doc.getElementsByTag("meta");
//            boolean findTitle = false;
//            boolean findThumbnail = false;
//            for(Element element : elements){
//                if(element.attr("name").equals("title")){
//                    title = element.attr("content");
//                    findTitle = true;
//                }
//                if(element.attr("property").equals("og:image")){
//                    thumbnail = element.attr("content");
//                    findThumbnail = true;
//                }
//                if(findTitle && findThumbnail){
//                    break;
//                }
//            }
//        } catch (IOException e) {
//            Log.e(getClass().getSimpleName(), e.getStackTrace()[0].toString());
//        }
//        String[] result = new String[2];
//        result[0] = title;
//        result[1] = thumbnail;
//        return result;
//    }

}
