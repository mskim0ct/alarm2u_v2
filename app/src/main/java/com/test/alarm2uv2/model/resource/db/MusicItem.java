package com.test.alarm2uv2.model.resource.db;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.test.alarm2uv2.BR;

import java.util.Objects;

@Entity(tableName = "music_table")
public class MusicItem extends BaseObservable implements Parcelable {
    String contentUri;
    @ColumnInfo(name="music_title")
    String musicTitle;
    String artist;

    public MusicItem(){}

    @Ignore
    public MusicItem(String contentUri, String musicTitle, String artist){
        this.contentUri = contentUri;
        this.musicTitle = musicTitle;
        this.artist = artist;
    }
    @Ignore
    protected MusicItem(Parcel in) {
        contentUri = in.readString();
        musicTitle = in.readString();
        artist = in.readString();
    }
    @Ignore
    public static final Creator<MusicItem> CREATOR = new Creator<MusicItem>() {
        @Override
        public MusicItem createFromParcel(Parcel in) {
            return new MusicItem(in);
        }

        @Override
        public MusicItem[] newArray(int size) {
            return new MusicItem[size];
        }
    };

    public String getContentUri() {
        return contentUri;
    }

    public void setContentUri(String contentUri) {
        this.contentUri = contentUri;
    }

    @Bindable
    public String getMusicTitle() {
        return musicTitle;
    }

    public void setMusicTitle(String musicTitle) {
        this.musicTitle = musicTitle;
        notifyPropertyChanged(BR.musicTitle);
    }

    @Bindable
    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
        notifyPropertyChanged(BR.artist);
    }
    @Ignore
    @Override
    public String toString() {
        return "MusicItem{" +
                "contentUri='" + contentUri + '\'' +
                ", msuicTitle='" + musicTitle + '\'' +
                ", artist='" + artist + '\'' +
                '}';
    }
    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }
    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentUri);
        dest.writeString(musicTitle);
        dest.writeString(artist);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MusicItem musicItem = (MusicItem) o;
        return Objects.equals(contentUri, musicItem.contentUri) &&
                Objects.equals(musicTitle, musicItem.musicTitle) &&
                Objects.equals(artist, musicItem.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contentUri, musicTitle, artist);
    }
}
