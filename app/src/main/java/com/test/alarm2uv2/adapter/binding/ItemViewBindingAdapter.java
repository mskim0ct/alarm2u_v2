package com.test.alarm2uv2.adapter.binding;

import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.slider.Slider;
import java.util.Calendar;
import java.util.Locale;

public class ItemViewBindingAdapter {
    /* 날짜 | 요일 표시 관련 */
    @BindingAdapter("bind:text_time")
    public static void setTextTime(TextView textView, long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        textView.setText(String.format(Locale.getDefault(), "%02d : %02d", hour, minute));
    }

    @BindingAdapter({"bind:day","bind:time"})
    public static void setDays(TextView textView, int[] days, long time){
        if(days == null){ //첫시작
            return ;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String[] arr = new String[]{"일", "월", "화", "수", "목", "금", "토"};
        boolean find = false;
        for(int i = 0 ; i <7 ; i++){
          if(days[i] == 1){
              find = true;
              break;
          }
        }
        String result = "";
        if(find){
            for(int i = 0 ; i < 7 ; i++){
                result = days[i] == 1 ? result+arr[i]:result;
            }
        }else{
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH)+1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            result = String.format(Locale.getDefault(),"%d / %02d / %02d",year,month,day);
        }
       textView.setText(result);
    }

    /*timepicker 관련*/
    @BindingAdapter("bind:hour")
    public static void setHour(TimePicker timePicker, int hour){
        timePicker.setHour(hour);
    }
    @InverseBindingAdapter(attribute = "bind:hour")
    public static int getHour(TimePicker timePicker){
        return timePicker.getHour();
    }
    @BindingAdapter("bind:minute")
    public static void setMinute(TimePicker timePicker, int minute){
        timePicker.setMinute(minute);
    }
    @InverseBindingAdapter(attribute = "bind:minute")
    public static int getMinute(TimePicker timePicker){
        return timePicker.getMinute();
    }
    @BindingAdapter(value={"android:onTimeChanged", "bind:hourAttrChanged", "bind:minuteAttrChanged"}, requireAll = false)
    public static void setTimeListener(TimePicker timePicker, TimePicker.OnTimeChangedListener listener
            , InverseBindingListener hourChange, InverseBindingListener minuteChange){
        if(hourChange == null && minuteChange == null){
            timePicker.setOnTimeChangedListener(listener);
        }else{
            timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                @Override
                public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                    if(listener != null){
                        listener.onTimeChanged(view, hourOfDay, minute);
                    }
                    if (hourChange != null) {
                        hourChange.onChange();
                    }
                    if(minuteChange != null){
                        minuteChange.onChange();
                    }
                }
            });
        }
    }
    /*MaterialButton + MaterialButtonToggleGroup 관련 : DAY */
    @BindingAdapter("bind:setChecked")
    public static void setChecked(MaterialButtonToggleGroup group, int[] arr){
        if(arr == null){
            return ;
        }
        for( int indx = 0 ; indx < group.getChildCount() ; indx++){
            MaterialButton childBtn = (MaterialButton)group.getChildAt(indx);
            childBtn.setChecked(arr[indx] == 1);
        }
    }
    @InverseBindingAdapter(attribute = "bind:setChecked", event="bind:checkAttrChanged")
    public static int[] getChecked(MaterialButtonToggleGroup group){
        int[] arr = new int[7];
        for( int indx = 0 ; indx < group.getChildCount() ; indx++){
            MaterialButton childBtn = (MaterialButton)group.getChildAt(indx);
            if(childBtn.isChecked()){
                arr[indx] = 1;
            }else{
                arr[indx] = 0;
            }
        }
        return arr;
    }
    @BindingAdapter(value="bind:checkAttrChanged")
    public static void setCheckListener(MaterialButtonToggleGroup group, InverseBindingListener checkAttrChanged){
        group.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                checkAttrChanged.onChange();
            }
        });
    }
    /*MaterialButton + MaterialButtonToggleGroup 관련 : REPEAT TIME */
//    @BindingAdapter("bind:setTimeChecked")
//    public static void setTimeChecked(MaterialButtonToggleGroup group, SnoozeInfo snoozeInfo){
//        int[] arr = {5,10,30};
//        for(int i = 0 ; i < 3 ; i++){
//            MaterialButton childBtn = (MaterialButton)group.getChildAt(i);
//            if(arr[i] == snoozeInfo.getTime()){
//                childBtn.setChecked(true);
//                break;
//            }
//        }
//    }
//    @InverseBindingAdapter(attribute = "bind:setTimeChecked", event = "bind:checkAttrChanged")
//    public static SnoozeInfo getTimeChecked(MaterialButtonToggleGroup group){
//        int[] arr = {5,10,30};
//        for(int i = 0 ; i < 3 ; i++){
//            MaterialButton childBtn = (MaterialButton)group.getChildAt(i);
//            if( childBtn.isChecked()){
//                return new SnoozeInfo(true,arr[i]);
//            }
//        }
//        return new SnoozeInfo(false, 0);
//    }

    /*slider*/
    @BindingAdapter("bind:sliderValue")
    public static void setSliderValue(Slider slider, int value){
        slider.setValue((float)value);
    }
    @InverseBindingAdapter(attribute = "bind:sliderValue", event ="sliderValueAttrChanged")
    public static int getSliderValue(Slider slider){
        Log.d("data binding adapter","slider : value "+slider.getValue());
        return (int)slider.getValue();
    }
    @BindingAdapter("sliderValueAttrChanged")
    public static void setSliderListener(Slider slider, InverseBindingListener sliderValueAttrChanged){
        slider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                if(fromUser){
                    slider.setValue(value);
                }

                sliderValueAttrChanged.onChange();
            }
        });
    }

//    /*edit text - enter key listener*/
//    @BindingAdapter("bind:oneLineEditText")
//    public static void setOneLineEditText(TextInputEditText textInputEditText, String value){
//        textInputEditText.setText(value == null? "" :value);
//    }
//    @InverseBindingAdapter(attribute = "bind:oneLineEditText", event="editTextActionChanged")
//    public static String getOneLineEditText(TextInputEditText textInputEditText){
//        return textInputEditText.getText() == null ? "" : textInputEditText.getText().toString();
//    }
//    @BindingAdapter("editTextActionChanged")
//    public static void setOnEditTextActionListener(TextInputEditText textInputEditText, InverseBindingListener editTextActionChanged){
//        textInputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Log.d("editor", v.getText().toString());
//                if(actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND){
//                    Log.d("editor","done .");
//                    editTextActionChanged.onChange();
//                    return true;
//                }
//                return false;
//            }
//        });
//    }

    public void setOnEditorActionListener(){

    }

}
