package com.test.alarm2uv2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.test.alarm2uv2.BR;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.databinding.LayoutMusicItemBinding;
import com.test.alarm2uv2.databinding.ViewMusicAlertBinding;
import com.test.alarm2uv2.model.resource.db.MusicItem;

public class MusicItemAdapter extends ListAdapter<MusicItem, MusicItemAdapter.MusicItemViewHolder> {

    public OnItemListener listener;
    private static final DiffUtil.ItemCallback<MusicItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<MusicItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull MusicItem oldItem, @NonNull MusicItem newItem) {
            return oldItem.getContentUri().equals(newItem.getContentUri());
        }

        @Override
        public boolean areContentsTheSame(@NonNull MusicItem oldItem, @NonNull MusicItem newItem) {
            return oldItem.getMusicTitle().equals(newItem.getMusicTitle()) && oldItem.getArtist().equals(newItem.getArtist());
        }
    };

    public MusicItemAdapter(OnItemListener listener) {
        super(DIFF_CALLBACK);
        this.listener = listener;
    }

    @NonNull
    @Override
    public MusicItemAdapter.MusicItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMusicItemBinding binding = LayoutMusicItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);

        return new MusicItemAdapter.MusicItemViewHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicItemAdapter.MusicItemViewHolder holder, int position) {
        MusicItem musicItem = getItem(position);
        holder.bind(musicItem);
    }

    public class MusicItemViewHolder extends RecyclerView.ViewHolder {
        private final LayoutMusicItemBinding binding;
        private final OnItemListener listener;
        public MusicItemViewHolder(LayoutMusicItemBinding binding, OnItemListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
            binding.getRoot().setOnClickListener(v -> {
                int position = getAdapterPosition();
                if(position != RecyclerView.NO_POSITION && this.listener != null) {
                    this.listener.onItemClick(getItem(position));
                }
            });
        }
        public void bind(MusicItem musicItem){
            binding.setVariable(BR.musicItem, musicItem);
            //TODO - 지워보기
            binding.executePendingBindings();
        }
    }
    public interface OnItemListener{
        void onItemClick(MusicItem musicItem);
    }

}
