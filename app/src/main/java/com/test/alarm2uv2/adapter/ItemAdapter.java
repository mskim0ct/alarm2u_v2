package com.test.alarm2uv2.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.test.alarm2uv2.R;
import com.test.alarm2uv2.common.CommonCode;
import com.test.alarm2uv2.databinding.LayoutItemBinding;
import com.test.alarm2uv2.model.resource.db.Item;


public class ItemAdapter extends ListAdapter<Item, ItemAdapter.ViewHolder>{
    public static final String TAG = "ItemAdapter.class";

    private final SwitchListener switchListener;
    private static final DiffUtil.ItemCallback<Item> DIFF_CALLBACK = new DiffUtil.ItemCallback<Item>() {
        @Override
        public boolean areItemsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.equals(newItem);
        }
    };

    public ItemAdapter(SwitchListener switchListener){
        super(DIFF_CALLBACK);
        this.switchListener = switchListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder 호출");
        if(viewType == CommonCode.FOOTER){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent,false);
            return new FooterViewHolder(v);
        }else{
            LayoutItemBinding binding = LayoutItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new ItemViewHolder(binding, parent.getContext());
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(holder instanceof  FooterViewHolder){
            FooterViewHolder footerViewHolder = (FooterViewHolder)holder;
        }else{
            ItemViewHolder itemViewHolder = (ItemViewHolder)holder;
            itemViewHolder.bind(getItem(position));
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount()+1;
    }

    @Override
    public Item getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType 호출 ... ");
        if(position == super.getItemCount()){
            return CommonCode.FOOTER;
        }
        return super.getItemViewType(position);
    }

    //ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    //Footer 를 위한 ViewHolder
    public class FooterViewHolder extends ViewHolder{

        public FooterViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    //content 를 위한 ViewHolder
    public class ItemViewHolder extends ViewHolder{
        LayoutItemBinding layoutItemBinding;
        Context context;
        public ItemViewHolder(LayoutItemBinding layoutItemBinding, Context context){
            super(layoutItemBinding.getRoot());
            this.context = context;
            layoutItemBinding.switchAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    if(switchListener != null && position != RecyclerView.NO_POSITION){
                        Item item  = getItem(position);
                        item.setRunFlag(isChecked);
                        switchListener.onChanged(item, buttonView.isPressed());
                        bind(item);
                    }
                }
            });
            this.layoutItemBinding = layoutItemBinding;
        }
        public void bind(Item item){
            //setThumbnail(item.getYouTubeItem().getThumbnail());
            Glide.with(context).load(item.getYouTubeItem().getThumbnail())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(layoutItemBinding.imageThumbnail);
            layoutItemBinding.setItem(item);
            layoutItemBinding.executePendingBindings();
        }

/*        public void setThumbnail(String value){
            SingleObserver<Bitmap> observer = new SingleObserver<Bitmap>(){
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    Log.d(TAG, "onSubscribe");
                }

                @Override
                public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Bitmap bitmap) {
                    Log.d(TAG, "onSuccess");
                    if(bitmap == null){
                        layoutItemBinding.imageThumbnail.setImageDrawable(context.getDrawable(R.drawable.default_image));
                    }else{
                        layoutItemBinding.imageThumbnail.setImageBitmap(bitmap);
                    }
                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    Log.d(TAG, "onError : "+e.getStackTrace()[0]);
                    layoutItemBinding.imageThumbnail.setImageDrawable(context.getDrawable(R.drawable.default_image));
                }
            };
            Single.fromCallable(new Callable<Bitmap>() {
                @Override
                public Bitmap call() throws Exception {
                    URL url = new URL(value);
                    Bitmap bitmap = null;
                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                    if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                        bitmap = BitmapFactory.decodeStream(conn.getInputStream());
                    }
                    conn.disconnect();
                    return bitmap;
                }
            }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(observer);
        }*/
    }

   public interface SwitchListener{
        //user : 사람에 의해서 switch가 변경되었는지에 대한 파라미터
       void onChanged(Item item, boolean user);
   }

}
