package com.test.alarm2uv2.converter.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.test.alarm2uv2.model.resource.db.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Converters{

    @TypeConverter
    public static int[] stringToArr(String value){
        int[] result = new int[7];
        for(int i = 0 ; i < 7 ; i++){
            result[i] = value.charAt(i)-'0';
        }
        return result;
    }

    @TypeConverter
    public static String arrToString(int[] arr){
        StringBuilder result = new StringBuilder();
        for(int e: arr){
            result.append(e);
        }
        return result.toString();
    }

    @TypeConverter
    public static String listToString(List<Item> list){
        Gson gson = new Gson();
        return gson.toJson(list);
    }
    @TypeConverter
    public static List<Item> stringToList(String str){
        if(str == null){
            return Collections.emptyList();
        }
        Gson gson = new Gson();
        return gson.fromJson(str, new TypeToken<List<Item>>(){}.getType());
    }

}
