package com.test.alarm2uv2;

import android.app.NotificationChannel;
import android.app.NotificationManager;

import androidx.core.app.NotificationManagerCompat;

public class Application extends android.app.Application {
    public static final String NOTIFICATION_ALARM_CHANNEL_ID="ALARM_CHANNEL_ID";
    public static final String NOTIFICATION_ALARM_ERROR_CHANNEL_ID="ALARM_ERROR_CHANNEL_ID";
    public static final String NOTIFICATION_ALARM_SERVICE_CHANNEL_ID="ALARM_SERVICE_CHANNEL_ID";
    @Override
    public void onCreate() {
        super.onCreate();
        //Notification 채널 등록
        createNotificationChannel(NOTIFICATION_ALARM_CHANNEL_ID, "유튜브 영상 알람 채널", NotificationManager.IMPORTANCE_HIGH,"firing alarm with full screen");
        createNotificationChannel(NOTIFICATION_ALARM_ERROR_CHANNEL_ID, "유튜브 영상 에러 알람 채널", NotificationManager.IMPORTANCE_DEFAULT, "play error youtube|music alarm.");
        createNotificationChannel(NOTIFICATION_ALARM_SERVICE_CHANNEL_ID, "유튜브 포어그라운드 서비스 채널", NotificationManager.IMPORTANCE_DEFAULT, "youtube play view foreground service.");

    }
    private void createNotificationChannel(String channelId, String channelName, int priority, String description){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, priority);
            notificationChannel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

}
