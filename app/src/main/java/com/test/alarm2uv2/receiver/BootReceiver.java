package com.test.alarm2uv2.receiver;

import android.app.Application;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.JobIntentService;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import com.test.alarm2uv2.common.Util;
import com.test.alarm2uv2.common.alarm.AlarmController;
import com.test.alarm2uv2.model.ItemRepository;
import com.test.alarm2uv2.model.resource.db.Item;
import com.test.alarm2uv2.service.CalcAlarmAtBootWorker;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

//TODO - 이후에 android.intent.action.CUSTOM_BOOT_COMPLETED 지우기 + AndroidManifest.xml 에도 지우기
public class BootReceiver extends BroadcastReceiver {

    private final String TAG = "BootReceiver.class";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "start boot receiver(alarm2uv2)");
        if(intent.getAction() !=null && (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")
                || intent.getAction().equals("android.intent.action.CUSTOM_BOOT_COMPLETED") )){

            WorkRequest calcAlarmRequest = OneTimeWorkRequest.from(CalcAlarmAtBootWorker.class);
            WorkManager.getInstance(context).enqueue(calcAlarmRequest);
        }
    }

}
