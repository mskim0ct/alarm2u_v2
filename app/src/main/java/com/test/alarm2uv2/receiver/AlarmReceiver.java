package com.test.alarm2uv2.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.test.alarm2uv2.service.YouTubeMainService;
import com.test.alarm2uv2.common.CommonCode;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction() == null || !intent.getAction().equals(context.getPackageName()+".ALARM_RECEIVER")){
            return ;
        }

        //YouTubeMainActivity 호출을 위한 PendingIntent 설정
        Bundle bundle =intent.getBundleExtra(CommonCode.ITEM);
        Intent serviceIntent = new Intent(context, YouTubeMainService.class);
        serviceIntent.putExtras(bundle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(serviceIntent);
        }else{
            context.startService(serviceIntent);
        }



/*
        Item item = (Item)bundle.get(CommonCode.ITEM);
        //Item item = (Item)intent.getParcelableExtra(CommonCode.ITEM);
        Intent youtubeIntent = new Intent(context, YouTubeMainActivity.class);
        youtubeIntent.putExtra(CommonCode.ITEM, item);
        youtubeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1001, youtubeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Notification 설정
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Application.NOTIFICATION_ALARM_CHANNEL_ID);
        builder.setContentTitle("알림")
                .setSmallIcon(R.drawable.ic_small_alarm)
                .setContentText("알림이 울렸습니다.")
                .setPriority(NotificationCompat.PRIORITY_HIGH) //API 25 이하에서 알림에 대한 중요도 설정
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setFullScreenIntent(pendingIntent, true)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(CommonCode.NOTIFICATION_ALARM_ID, builder.build());
*/

    }
}
